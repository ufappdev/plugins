/*
    ReplacementDropdown is a QuillJS Module that creates a custom toolbar with text replacements 
    specified by a LOV
*/
class ReplacementDropdown {
    constructor(quill, options) {
        this.quill = quill;
        this.options = options;
        this.container = $(`${options.container}`);
        this.textReplacementList = options.textReplacementList;
        this.textReplacements = this.textReplacementList.split(":");
        this.textTooltipList = options.textTooltipList;
        this.initializeDropdown();
        this.initializeDropdownTooltips();

        let selfQuill = this.quill;
        // TODO move this out into its own function, need to update the scope
        $(".ql-dropdownContainer .ql-picker-item").on("click", function() {
            let selection = selfQuill.getSelection(true);       // Get the selection
            let cursorPosition = selection.index;               // Get the cursor position
            let content = $(this).attr("data-content");         // Get the content of the dropdown item

            if ((selfQuill.getLength() + content.length) <= maxCount) {
                selfQuill.deleteText(selection.index, selection.length);                            // Delete the text that was selected
                selfQuill.clipboard.dangerouslyPasteHTML(cursorPosition,`${content}`, "api");       // Insert the selected option
                $('.ql-dropdownContainer.ql-picker').toggleClass("ql-expanded");                    // Hide the dropdown
                selfQuill.setSelection(cursorPosition + content.length);                            // Move the cursor to the end of the inserted text
            }
        });
    }

    initializeDropdown() {
        // Set the picker label to the default value
        this.setPickerLabel("Select a label");

        // For each of the text replacements
        this.textReplacements.forEach((el, index) => { 
            $(`.ql-dropdownContainer.ql-picker > .ql-picker-options`).append(this.getPickerItem(index + 1));  // Append the picker item to the dropdown options
            $(`.ql-dropdownContainer .ql-picker-item[data-value='${index + 1}']`).attr("data-content", el);   // Update the text of the dropdown item
        });

        // Allows the tooltip descriptions to show beyond the width of the editor
        // Strangely though, this dependency is dynamic and can rely on EITHER
        // of the two parent elements used
        this.container.closest('.t-Form-inputContainer').css("overflow", "visible");
        this.container.closest('.rel-col').css("overflow", "visible");
    }

    initializeDropdownTooltips() {
        // For each tooltip description
        this.textTooltipList.forEach((el, index) => {
            $(`.ql-dropdownContainer .ql-picker-item[data-value='${index + 1}']`).attr("data-tooltip", el);   // Update the text of the dropdown item description
        });
    }

    setPickerLabel(replacementText) {
        // Set the dropdown label
        $(`.ql-dropdownContainer .ql-picker-label`).attr("data-content", replacementText);
    }

    getPickerItem(value) {
        return `<span class="ql-picker-item" data-value="${value}"></span>`
    }
}

Quill.register('modules/replacement_dropdown', ReplacementDropdown);