"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var orgChart = [];
var root = "";
var children = "";
var chartDepth = 0;
var minimumDepth = 2; // TODO Create method for adding the necessary html for a node
// Move out the Person class and import

function initializeChart(personArray, root) {
  // printDivider("ROOT");
  createChart(personArray);
  assignChildren();
  /*
  |  With how the orgchart library works it needs a root node specified 
  |  SEPARATE from the children. Therefore we need to split the two and
  | pass them in as different objects.
  */
  // Get the children

  children = orgChart[0].getChildren();
  var childJSON = getJSON(children);
//   console.log("Children: " + children);
//   console.log(JSON.stringify(childJSON)); // Get the root

  var oldRoot = orgChart[0];
  root = new Person("", "", oldRoot.getName(), oldRoot.getTitle());
//   console.log(root);
  root.removeChildren(); // Display the chart

  displayChart(root, childJSON);
}

function createChart(personArray) {
  printDivider("CREATE CHART");
  var count = 0;
  var parentKeyList = [];
  var primaryKey = "";
  var parentKey = "";
  var nodeDepth = 0; // Create an instance of Person for each index in the personArray

  personArray.forEach(function (element) {
    var _console;

    // let attributes = element.split("\n");       // Get the attributes
    var attributes = element.split(":"); // Get the attributes

    // (_console = console).log.apply(_console, _toConsumableArray(attributes));

    var person = _construct(Person, _toConsumableArray(attributes)); // Create a person with the attributes


    orgChart.push(person); // Add the person to the organization chart
    // Get the primaryKey and parentKey

    primaryKey = attributes[0];
    parentKey = attributes[1]; // Get the max depth

    nodeDepth = person.getLevel();
    chartDepth = chartDepth > nodeDepth ? chartDepth : nodeDepth;
  });
//   console.log(orgChart);
}

function include(arr, obj) {
  return arr.indexOf(obj) != -1;
} // Assigns the correct children to each node


function assignChildren() {
  printDivider("ASSIGN CHILDREN");
//   console.log(orgChart);
  var searchKey = "";
  var indexesToRemove = [];
  orgChart.forEach(function (parent) {
    // Get the key to search for in the children
    var searchKey = parent.getPrimaryKey(); // Search through the rest to see if they have a parent

    // console.log('\n');
    // console.log("PARENT: " + parent.getName());
    // console.log("###########################");
    // console.log('\n');
    orgChart.forEach(function (child) {
    //   console.log("child: " + child.getName());

      if (child.hasParent(searchKey)) {
        parent.addChild(child); // Remove from root orgchart

        // console.log(child.getName() + " is the child of:  " + parent.getName());
        var index = orgChart.indexOf(child);
        indexesToRemove.push(index);
      }
    });
  });
//   console.log("Need to remove these indices: " + indexesToRemove); // Create a new array to hold the updated org chart

  var newOrgChart = []; // Create the organization chart with the updated parent/child relationships

  orgChart.forEach(function (element, ind) {
    if (indexesToRemove.indexOf(ind) == -1) {
      newOrgChart.push(element);
    }
  }); // Assign the org chart to the updated chart

  orgChart = newOrgChart;
  newOrgChart = "";
//   console.log(orgChart);
} // TODO: UNUSED


function isMiddleLevel(person) {
//   console.log(person.toString());
  return person.getLevel().includes(LEVELS[2]);
} // Gets the JSON of all the nodes within a tree


function getJSON(tree) {
  printDivider("CREATE JSON");
//   console.log(tree);
  var info = [];
  tree.forEach(function (element) {
    info.push(element.getJSON());
  });
  return info;
} // TODO: UNUSED


function assignLevel() {
  printDivider("ASSIGN LEVELS");
//   console.log(orgChart); // For every top level person iterate through the children

  orgChart.forEach(function (person) {
    // console.log("Iterating over " + person.getName());
    iterate(person, 0);
  });
}

function displayChart(root, children) {
  var datasource = {
    "name": "".concat(root.getName()),
    "title": "<p class=\"name\">".concat(root.getName(), "</p> <p>").concat(root.getTitle(), "</p>"),
    "className": LEVELS[0],
    "children": children
  };
//   console.log("chart depth: " + chartDepth);
  var chartContainer = $('#chart-container');
  var oc = $('#chart-container').orgchart({
    // "chartContainer": "#chart-container",
    "data": datasource,
    // "exportButton": true,
    // "exportFilename": 'OrgChart',
    // "exportFileextension": 'pdf',
    "verticalDepth": chartDepth <= minimumDepth ? minimumDepth + 1 : minimumDepth + 1,
    "depth": chartDepth <= minimumDepth ? minimumDepth : chartDepth,
    //         "pan": true,
    //"zoom": true,
    "nodeContent": "title" // 'toggleSiblingsResp': true,
    // 'createNode': function($node, data) {
    //   $node.on('click', function(event) {
    //     if (!$(event.target).is('.edge, .toggleBtn')) {
    //       var $this = $(this);
    //       var $chart = $this.closest('.orgchart');
    //       var newX = window.parseInt(($chart.outerWidth(true)/2) - ($this.offset().left - $chart.offset().left) - ($this.outerWidth(true)/2));
    //       var newY = window.parseInt(($chart.outerHeight(true)/2) - ($this.offset().top - $chart.offset().top) - ($this.outerHeight(true)/2));
    //       $chart.css('transform', 'matrix(1, 0, 0, 1, ' + newX + ', ' + newY + ')');
    //     }
    //   });
    // }

  }); // orgchart = new OrgChart({
  //     "chartContainer": "#chart-container",
  //     "data": datasource,
  //     "depth": chartDepth - 1,
  //     // "pan": true,
  //     // "zoom": true,
  //     "nodeContent": "title",
  //     // "exportButton": true,
  //     // "exportFilename": 'OrgChart'
  // }); 

  $('#chart-container').on('touchmove', function (event) {
    event.preventDefault();
  }); // oc.setChartScale($('.orgchart'),0.5);
}

var LEVELS = {
  0: "root-level",
  1: "top-level",
  2: "middle-level",
  3: "bottom-level",
  4: "low-level",
  5: "lowest-level"
};

var Person =
/*#__PURE__*/
function () {
  function Person(primaryKey, parentKey, name, title, level) {
    _classCallCheck(this, Person);

    this.primaryKey = primaryKey;
    this.parentKey = parentKey;
    this.name = name;
    this.title = title;
    this.children = [];
    this.level = this.calculateLevel(level);
    this.className = LEVELS[this.calculateLevel(level)];
  }

  _createClass(Person, [{
    key: "calculateLevel",
    value: function calculateLevel(level) {
      var MAX_LEVEL = 5;
      return level > MAX_LEVEL ? MAX_LEVEL : level;
    }
  }, {
    key: "getJSONstr",
    value: function getJSONstr() {
      return this.JSON_str;
    }
  }, {
    key: "getPrimaryKey",
    value: function getPrimaryKey() {
      return this.primaryKey;
    }
  }, {
    key: "getParentKey",
    value: function getParentKey() {
      return this.parentKey;
    }
  }, {
    key: "getName",
    value: function getName() {
      return this.name;
    }
  }, {
    key: "getTitle",
    value: function getTitle() {
      return this.title;
    }
  }, {
    key: "hasParent",
    value: function hasParent(key) {
    //   console.log("key: " + key);
      if (key) {
        if (key === this.parentKey) return true;
      }

      return false;
    }
  }, {
    key: "addChild",
    value: function addChild(child) {
      this.children.push(child);
    }
  }, {
    key: "getChildren",
    value: function getChildren() {
      return this.children;
    }
  }, {
    key: "hasChildren",
    value: function hasChildren() {
      return this.children.length === 0 ? false : true;
    }
  }, {
    key: "removeChildren",
    value: function removeChildren() {
      this.children = "";
    }
  }, {
    key: "getLevel",
    value: function getLevel() {
      return this.level;
    } // toString() {
        // console.log(`Primary key: ${this.primaryKey}`);
        // console.log(`Parent key: ${this.parentKey}`);
        // console.log(`Name: ${this.name}`);
        // console.log(`Title: ${this.title}`);
        // console.log(`Children: ${this.children}`);
    // }

  }, {
    key: "getJSON",
    value: function getJSON() {
      if (!this.hasChildren()) {
        return {
          name: "".concat(this.name),
          // title: this.title,
          title: "<p class=\"name\">".concat(this.name, "</p> <p>").concat(this.title, "</p>"),
          className: this.className
        };
      } else {
        return {
          name: "".concat(this.name),
          title: "<p class=\"name\">".concat(this.name, "</p> <p>").concat(this.title, "</p>"),
          className: this.className,
          children: this.iterate(this)
        };
      }
    } // Update this

  }, {
    key: "iterate",
    value: function iterate(current) {
      var children = current.getChildren();
      var json_str = [];
      var child = "";

      for (var i = 0, len = children.length; i < len; i++) {
        child = children[i];
        json_str.push(child.getJSON());
        this.iterate(children[i]);
      }

      return json_str;
    }
  }]);

  return Person;
}();

function printDivider(text) {
//   console.log('\n');
//   console.log("###########################");
//   console.log(text);
//   console.log("###########################");
}